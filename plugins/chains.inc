<?php
/**
 * @file
 * Provide Job Chain worker plugin.
 *
 * @author Jeremy Thorson ("jthorson", http://drupal.org/user/148199)
 */

$plugin = array(
  'title' => t('Conduit chains'),
  'description' => t('Provide a job chaining functionality plugin.'),
  'perform' => 'chains_perform',
);

/**
 * Perform the job.
 *
 * @param $properties
 *   Associative array of properties defining the job.
 * @return
 *   An array containing a boolean for pass/fail and the result.
 *
 * @see worker_perform()
 */
function chains_perform(array $properties) {
  // Since the chain job doesn't really do anything besides serve as a
  // placeholder in the main conduit queue, we simply return 'TRUE'.  The
  // magic all happens on the conduit side of the equation.

  // Return 'TRUE' to trigger next request and start the chain
  return array(TRUE, 'Job Chain Started');
}
